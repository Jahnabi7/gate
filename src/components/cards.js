import React, { Component } from "react";

class Row extends Component {
  render() {
    return (
      <div className="row">
        <Cards />
      </div>
    );
  }
}

class Cards extends Component {
  constructor() {
    super();
    this.state = {
      totalPrice: 0,
      items: [
        { id: 1, price: 2950, isAdded: false },
        { id: 2, price: 2950, isAdded: false },
        { id: 3, price: 2950, isAdded: false },
        { id: 4, price: 2950, isAdded: false },
        { id: 5, price: 2950, isAdded: false },
      ],//isAdded : false means button label is "Add", i.e the price is not added
    };
  }

  handleAddonClick(price, index) {
    let localState = this.state;
    localState.totalPrice = localState.totalPrice + price;
    localState.items[index].isAdded = true;
    this.setState(localState);
  }

  handleRemoveonClick(price, index) {
    let localState = this.state;
    localState.totalPrice = localState.totalPrice - price;
    localState.items[index].isAdded = false;
    this.setState(localState);
  }
  displayButton(item, index) {
    if (item.isAdded === true) {
      return (
        <button
          className="btn btn-outline-secondary"
          onClick={() => {
            this.handleRemoveonClick(item.price, index);
          }}
        >
          Remove
        </button>
      );
    } else {
      return (
        <button
          className="btn btn-outline-secondary"
          onClick={() => {
            this.handleAddonClick(item.price, index);
          }}
        >
          Add
        </button>
      );
    }
  }

  displayCard() {
    return this.state.items.map((item, index) => (
      <div key={item.id} className="col-4 col-sm-6 col-lg-4">
        <div className="card mb-3">
          <div className="card-body">
            <h5 className="card-title">Lorem Ipsum</h5>
            <p className="card-text">
              Quis dolore officia excepteur veniam nulla.
            </p>
            <p className="card-text">
              Quis dolore officia excepteur veniam nulla.
            </p>
            <br></br>
            <p className="card-text">
              Quis dolore officia excepteur veniam nulla.
            </p>
            <p className="card-text">
              <img src="/images/box.jpg" className="box" />
            </p>
            <p className="card-text">
              Price: {this.displayButton(item, index)}
              <br></br>$2950
            </p>
          </div>
        </div>
      </div>
    ));
  }
  render() {
    return (
      <>
        {this.displayCard()}
        <div className="container">Total Price: $ {this.state.totalPrice}</div>
      </>
    );
  }
}

export default Row;
