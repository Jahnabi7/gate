import React, {Component} from "react";

class Footer extends Component {
    render() {
        return (
            <div className="jumbotron">
        <div className="container footer">
          <div className="row">
            <div className="col">
              <img src="/images/b.png" className="c" />
            </div>
            <div className="col">
              <p>
                <b>Lorem Ipsum</b>
              </p>

              <p>
                <a className="text-muted">Lorem Ipsum Dolor</a>
                <br></br>
                <a className="text-muted">Lorem Ipsum Dolor</a>
                <br></br>
                <a className="text-muted">Lorem Ipsum Dolor</a>
                <br></br>
                <a className="text-muted">Lorem Ipsum Dolor</a>
              </p>
            </div>
            <div className="col">
              <p>
                <b>Lorem Ipsum</b>
              </p>

              <p>
                <a className="text-muted">Lorem Ipsum Dolor</a>
                <br></br>
                <a className="text-muted">Lorem Ipsum Dolor</a>
                <br></br>
                <a className="text-muted">Lorem Ipsum Dolor</a>
                <br></br>
                <a className="text-muted">Lorem Ipsum Dolor</a>
              </p>
            </div>
            <div className="col">
              <p>
                <b>Lorem Ipsum</b>
              </p>

              <p>
                <a className="text-muted">Lorem Ipsum Dolor</a>
                <br></br>
                <a className="text-muted">Lorem Ipsum Dolor</a>
                <br></br>
                <a className="text-muted">Lorem Ipsum Dolor</a>
                <br></br>
                <a className="text-muted">Lorem Ipsum Dolor</a>
              </p>
            </div>
            <div className="col">
              <p>
                <b>Contact Us</b>
              </p>

              <p className="text-muted">
                THE GATE ACADEMY (HO)<br></br>
                Vivekananda Nagar #3, 11th A CrossRd,<br></br>
                near Jockey Factory, Bengaluru,<br></br>
                Karnataka 560068, INDIA
              </p>
            </div>
          </div>
          <hr className="my-4"></hr>
          <div className="row">
            <div className="col">
              <img src="/images/fb.png" className="img" />
              <img src="/images/ins.png" className="img" />
              <img src="/images/tw.png" className="img" />
              <img src="/images/pin.png" className="img" />
            </div>
            <div className="col blog text-muted">
              Services Portfolio Pricing Testimonials Team Blog Career
            </div>
            <div className="col">
              <span className="text-muted">
                <img src="/images/p.png" className="img" />
                +918040611000{" "}
              </span>
              <span className="text-muted">
                <img src="/images/m.png" className="img" />
                info@gateacademy.com
              </span>
            </div>
          </div>
        </div>
      </div>

        )
    }
}

export default Footer;