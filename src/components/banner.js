import React, { Component } from "react";
import Row from "./cards";

class Banner extends Component {
  render() {
    return (
      <div className="container">
        <BannerContent />
          <Row />
      </div>
    );
  }
}

class BannerContent extends Component {
  render() {
    return (
      <div className="jumbotron jumbotron-one">
        <h1>
          Amet consequat deserunt fugiat et qui velit ut ipsum velit irure
          occaecat.
        </h1>
        <p>
          Pariatur occaecat ut elit sunt est reprehenderit nisi sit aliquip
          nostrud laborum incididunt exercitation commodo. Sint cupidatat
          cupidatat non anim tempor cupidatat. Esse ut ex elit pariatur quis.
          Exercitation duis et occaecat culpa exercitation veniam dolore
          cupidatat commodo aute est.
        </p>

        <span className="btn">
          <button type="button" className="btn btn-primary">
            Subscribe
          </button>
        </span>

        <img src="/images/btn.png" className="play" alt="" />
        <span className="btn">
          Watch<br></br>
          <b>Intro Video</b>
        </span>
      </div>
    );
  }
}
 
export default Banner;
