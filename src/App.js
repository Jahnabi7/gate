import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import Banner from "./components/banner";
import Footer from "./components/footer";

class App extends Component {
  render() {
    return (
      <>
        <Banner />
        <Footer />
      </>
    );
  }
}

export default App;
